# Kuksa Container Image

Container image file to build Eclipse Kuksa components.

Only Kuksa Databroker is being packaged atm.

## Building the Container

Build the chariott container:
```bash
podman build -t localhost/kuksa:latest .
```

## License

[MIT](./LICENSE)
